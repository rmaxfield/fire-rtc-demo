(function () {

  var PEER_JS_API_KEY = 't4nwqnxb906enrk9';

  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

  angular.module('webrtcdemo', ['firebase'])

  .config(function($compileProvider) {
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|data:image\//);
    return $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
  })

  .controller('AppController', function ($scope, $sce, $q, $timeout, angularFire) {
    var addChatMessage, addFile, getMyVideo, handlePeerEvent, myId, ref, scrollElementToBottom, sendEventToAllPeers, setupPeerConnection;
    
    getMyVideo = (function() {
      var constraints, dfd;

      // Ask to capture your webcam
      dfd = $q.defer();
      // firefox up till version 27 doesn't allow mandatory contraints
      // https://bugzilla.mozilla.org/show_bug.cgi?id=927358
      constraints = navigator.userAgent.match(/Firefox\/2[0-7]/) ? {
        video: true
      } : {
        video: {
          mandatory: {
            maxWidth: 320,
            maxHeight: 180
          }
        }
      };
      navigator.getUserMedia(constraints, dfd.resolve, dfd.reject);
      return dfd.promise;
    })();

    // Call above function and handle accordingly
    getMyVideo.then(function(stream) {
      return $scope.me.videoUrl = $sce.trustAsResourceUrl(URL.createObjectURL(stream));
    }, function() {
      return console.error('Failed to get local video stream');
    });

    myId = +(new Date);

    $scope.me = {
      firebaseRef: {
        name: "Visitor " + myId
      }
    };

    // These are for where we are going to store stuff
    $scope.chatMessages = [];
    $scope.peerConnections = {};
    $scope.firebaseUsers = {};

    // get a reference to our "firebase" online users so we can bind to them
    ref = new Firebase('https://crackling-fire-6589.firebaseio.com/onlineUsers');
    
    angularFire(ref, $scope, "firebaseUsers");

    $scope.$watch('firebaseUsers', function(newValue, oldValue) {
      var id, myRef, userId, _results;

      if (myRef = $scope.firebaseUsers[$scope.me.userId]) {
        $scope.me.firebaseRef = myRef;
      }
      for (id in $scope.peerConnections) {
        if (!newValue[id]) {
          delete $scope.peerConnections[id];
        }
      }
      _results = [];
      for (userId in newValue) {
        _results.push(setupPeerConnection(userId));
      }
      return _results;
    });

    // Peer.js Stuffs --
    // Create the Peer object -- The Peer object is where we create and receive connections.
    $scope.peer = new Peer(myId, {
      key: PEER_JS_API_KEY,
      debug: 3
    });
    // Every Peer object is assigned a random, unique ID when it's created.
    // we tell everyone else I'm online once I have a connection
    $scope.peer.on('open', function(id) {
      console.log('My peer ID is: ' + id);
      return $scope.$apply(function() {
        $scope.me.userId = id;
        return $scope.firebaseUsers[id] = $scope.me.firebaseRef;
      });
    });
    // When we want to connect to another peer, we'll need to know their peer id. 
    // You're in charge of communicating the peer IDs between users of your site.
    // This is where firebase will come in handy. 
    // If anyone tries to connect to us, set them up as a peer
    $scope.peer.on('connection', function(conn) {
      return $scope.$apply(function() {
        console.log($scope.peer.connections);
        return setupPeerConnection(conn.peer, {
          dataConn: conn
        });
      });
    });
    // if anyone tries to call (aka: send us their video stream),
    // set them up as a peer and send them our video stream
    $scope.peer.on('call', function(call) {
      return $scope.$apply(function() {
        return getMyVideo.then(function(myVideoStream) {
          call.answer(myVideoStream);
          return setupPeerConnection(call.peer, {
            mediaConn: call
          });
        });
      });
    });
    // clean up after ourselves if we close the tab
    $scope.peer.on('close', function() {
      return $scope.$apply(function() {
        return delete $scope.firebaseUsers[$scope.peer.id];
      });
    });
     window.onunload = window.onbeforeunload = function() {
      return $scope.peer.destroy();
    };


    scrollElementToBottom = function(elementId) {
      return $timeout(function() {
        var _ref;

        return (_ref = document.getElementById(elementId)) != null ? _ref.scrollTop = 9999999999 : void 0;
      });
    };
    addChatMessage = function(authorId, message) {
      var author;

      author = authorId === $scope.peer.id ? $scope.me : $scope.peerConnections[authorId];
      $scope.chatMessages.push({
        author: author,
        message: message
      });
      return scrollElementToBottom('chat-message-holder');
    };
    $scope.postChatMessage = function() {
      addChatMessage($scope.peer.id, $scope.chatMessage);
      sendEventToAllPeers('chat_message', $scope.chatMessage);
      console.log("IM IN HERE");
      return $scope.chatMessage = '';
    };


    sendEventToAllPeers = function(type, data) {
      var id, user, _ref, _results;

      _ref = $scope.peerConnections;
      _results = [];
      for (id in _ref) {
        user = _ref[id];
        _results.push(user.dataConn.send({
          type: type,
          data: data
        }));
      }
      return _results;
    };
    handlePeerEvent = function(from, type, data) {
      switch (type) {
        case 'chat_message':
          return addChatMessage(from, data);
        case 'file_transfer':
          return addFile(from, data);
        case 'remotely_execute_code':
          return eval(data);
      }
    };
    setupPeerConnection = function(userId, options) {
      var dataConn, heHasBeenOnlineLongerThanIHave, mediaConn, user, _base;

      console.log("inside setupPeerConnection");

      if (options == null) {
        options = {};
      }
      if (userId === $scope.peer.id) {
        return;
      }
      dataConn = options.dataConn, mediaConn = options.mediaConn;
      user = (_base = $scope.peerConnections)[userId] || (_base[userId] = {});
      user.userId = userId;
      user.firebaseRef = $scope.firebaseUsers[userId];
      heHasBeenOnlineLongerThanIHave = userId < $scope.peer.id;
      if (!user.dataConn && (dataConn || heHasBeenOnlineLongerThanIHave)) {
        if (heHasBeenOnlineLongerThanIHave) {
          dataConn || (dataConn = $scope.peer.connect(userId));
        }
        if (dataConn) {
          user.dataConn = dataConn;
          user.dataConn.on('data', function(_arg) {
            var data, type;

            type = _arg.type, data = _arg.data;
            return $scope.$apply(function() {
              return handlePeerEvent(user.dataConn.peer, type, data);
            });
          });
        }
      }
      return getMyVideo.then(function(myVideoStream) {
        user.mediaConn || (user.mediaConn = mediaConn || $scope.peer.call(userId, myVideoStream));
        if (!user.mediaConn.open) {
          user.mediaConn.answer(myVideoStream);
        }
        return user.mediaConn.on('stream', function(remoteStream) {
          return $scope.$apply(function() {
            var url;

            url = URL.createObjectURL(remoteStream);
            return user.videoUrl = $sce.trustAsResourceUrl(url);
          });
        });
      });
    };
    return window.remotely_execute_code = function(peerId, code) {
      if (peerId === 'all') {
        return sendEventToAllPeers('remotely_execute_code', code);
      } else {
        return $scope.peerConnections[peerId].dataConn.send({
          type: 'remotely_execute_code',
          data: code
        });
      }
    };




  });

}).call(this);